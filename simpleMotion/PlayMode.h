#pragma once

#include "Wall.h"
#include "Ball.h"
#include "Gun.h"
#include "SFML/Graphics.hpp"
using namespace std;
class Game;

class PlayMode
{
public:
	PlayMode() : mpGame(nullptr) {}
	void Init(Game*);
	void Update();
	void Render();
	void ballCollision(Ball& mBall);
private:
	Game *mpGame; //for communication

	sf::Texture mCannonTex;			//cannon
	sf::Texture mWallTex;			//walls
	Wall mWalls[Wall::MAX_WALLS];	//four walls
	Gun mGun;		//cannon
	Ball mBall;
};