#pragma once
#include "GameObj.h"

class Ball : public GameObj
{
public:
	Ball()
		:GameObj()
	{}

	sf::Sprite createBall(bool& isFired);
	void fireBall(sf::Sprite& ball, bool& isFired);
	void update(sf::Sprite& ball, bool& isFired);
	~Ball();

	sf::Texture mCannonBallTex;	//Cannonball
	sf::Sprite ball;
	bool isFired = false;
	float ballRot;
	float angleAtFire;
	int fireRate = 0;
	float ballSpeedX = 1.f;
	float ballSpeedY = 1.f;
};