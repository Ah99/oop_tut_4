#include <assert.h>
#include "SFML/Graphics.hpp"
#include "Application.h"

using namespace std;

int main()
{
	Application app;
	app.Run();

	return EXIT_SUCCESS;
}
