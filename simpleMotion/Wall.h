#pragma once
#include "GameObj.h"


class Wall : public GameObj
{
public:
	static const int MAX_WALLS = 4;				//a wall on each side of the screen
	enum WallType { LEFT, RIGHT, TOP, BOTTOM };	//identify where each wall is

	Wall()
		:GameObj(), mType(LEFT)
	{}
	void Init(WallType wtype) {
		mType = wtype;
	}

	inline void Wall::Update()
	{
		
	}
private:
	WallType mType; //where does this instance fit on screen
};