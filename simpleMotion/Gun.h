#pragma once
#include "GameObj.h"

class Gun : public GameObj
{
public:
	Gun()
		:GameObj(), mWaitSecs(0)
	{}
	void Update();
private:
	float mWaitSecs;	//delay after firing beforeo you can move/fire
};