#include "assert.h"
#include "Ball.h"
#include "Game.h"
#include "PlayMode.h"
#include "Application.h"

void Ball::update(sf::Sprite& ball, bool& isFired)
{
	float inc = 0;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		inc = -GDC::PLAY_SPIN_SPD;
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		inc = GDC::PLAY_SPIN_SPD;

	if(!isFired)
		ball.setRotation(ball.getRotation() + inc * Application::GetElapsedSecs());		//Moves the ball around when not fired


	if (isFired)
	{
		fireBall(ball, isFired);
		ballRot = (ballRot + inc * Application::GetElapsedSecs());
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && fireRate < 10)
	{
		ballSpeedX = 1.0f;
		ballSpeedY = 1.0f;
		//fire the ball here
		fireRate = 1000;
		if (isFired) 
		{ 
			isFired = false; 
			ball.setRotation(ballRot); 
		}
		else { fireBall(ball, isFired); ballRot = ball.getRotation(); }
	}

	fireRate--;
}

sf::Sprite Ball::createBall(bool& isFired)
{
	if (!mCannonBallTex.loadFromFile("data/cannonball.png")) { assert(false); }
	mCannonBallTex.setSmooth(true);
	ball.setTexture(mCannonBallTex);
	ball.setOrigin(85, 63);
	if (!isFired)
	{
		ball.setPosition(GDC::SCREEN_RES.x / 2.f, GDC::SCREEN_RES.y / 2.f);
	}
	update(ball, isFired);
	return ball;
}

void Ball::fireBall(sf::Sprite& ball, bool& isFired)
{
	angleAtFire = ball.getRotation();		//Stores rotation cannon was at when it fired the ball
	isFired = true;
	float cosAngle, sinAngle;
	cosAngle = (angleAtFire * PI / 180);
	sinAngle = (angleAtFire * PI / 180);
	ball.move(ballSpeedX * -cos(cosAngle), ballSpeedY * -sin(sinAngle));		//Fires the cannon straight based on angle
}

Ball::~Ball()
{}